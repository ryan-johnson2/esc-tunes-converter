from argparse import ArgumentParser

# BLHeli_S / Bluejay uses RTTTL (https://en.wikipedia.org/wiki/Ring_Tone_Text_Transfer_Language)
#BLHeli_32 uses something else (maybe custom?)

class ESCTuneNote(object):

    def __init__(self, note=None, length=0, octave=0, default_len=8):
        self._note = note
        self._octave = int(octave)
        self._length = int(length)

        self._default_len = default_len

    def blheli_s_note(self):
        # uses RTTTL
        if self._note != "p":
            return f"{self._length}{self._note}{self._octave}"
        return f"{self._length}p"
    
    def blheli_32_note(self, scale=False, min_len=16):
        if self._note == "p":
            return f"P{self._length}"
        
        if not scale:
            return f"{self._note.upper()}{self._octave} {self._length}"
        
        scale_mod = min_len // 8

        print(scale_mod)
        if self._length >= scale_mod:
            return f"{self._note.upper()}{self._octave} {self._length // scale_mod}"
        
        whole_note_count = scale_mod // self._length
        note_string = ""
        for i in range(whole_note_count):
            note_string += f"{self._note.upper()}{self._octave} 1"

        return note_string
    
    def parse_rtttl_note(self, note_str):
        self._note = ""
        self._octave = 0
        self._length = 0

        ind = 0

        length_str = ""
        while note_str[ind].isdigit():
            length_str += note_str[ind]
            ind += 1

        if length_str:
            self._length = int(length_str)
        else:
            self._length = self._default_len

        if ind + 1 < len(note_str) and note_str[ind + 1] == "#":
            self._note = note_str[ind:ind+2]
            ind += 2
        else:
            self._note = note_str[ind]
            ind += 1

        octave = note_str[ind:]
        if octave:
            self._octave = int(octave)

    def parse_blheli_32_note(self, note_str):
        raise NotImplementedError

    def __str__(self):
        return self.blheli_s_note()
    
    def __repr__(self):
        return f"<ESCTuneNote Note={self._note} Octave={self._octave} Length={self._length}>"


class ESCTune(object):

    def __init__(self, tune_str=None):
        self._notes = []
        self._defaults = ""
        self._name = ""

        if tune_str:
            self._parse_tune(tune_str)
        # self._defaults

    def blheli_s_tune(self):
        raise NotImplementedError

    def blheli_32_tune(self, min_len=0):
        scale = False
        min_len = 8

        for note in self._notes:
            if note._length > min_len:
                scale = True
                min_len = note._length
            
        tune=""
        for note in self._notes:
           tune += f"{note.blheli_32_note(scale, min_len)} "

        return tune[:-1]

    def _parse_tune(self, tune_str):
        if ":" in tune_str:
            return self._parse_rtttl_tune(tune_str)
        else:
            return self._parse_blheli32_tune(tune_str)
        
    def _parse_rtttl_tune(self, tune_str):
        tune = []
        name, defaults, notes = tune_str.strip().split(":")
        default_len = 0

        for default in defaults.strip().split(","):
            if default[0] == "d":
                default_len = int(default[2:])

        for note in notes.strip().split(","):
            new_note = ESCTuneNote(default_len=default_len if default_len else 8)
            new_note.parse_rtttl_note(note)
            self._notes.append(new_note)    
    
    def _parse_blheli32_tune(self, tune_str):
        raise NotImplementedError
    

if __name__ == "__main__":
    parser = ArgumentParser()
    
    input_group = parser.add_mutually_exclusive_group(required=True)
    input_group.add_argument("--file", default=None, help="A file that contains line seperated ESC tunes, one line per ESC")
    input_group.add_argument("--tune", default=None, help="A single ESC tune passed on the command line")

    args = parser.parse_args()

    if args.file:
        tunes = []

        with open(args.file, "r") as f:
            f_tunes = f.readlines()

        for f_tune in f_tunes:
            tune = ESCTune(f_tune)
            tunes.append(tune)

        with open(f"{args.file}.bl32", "w+") as f:
            for tune in tunes:
                f.write(f"{tune.blheli_32_tune()}\n")
    else:
        tune = ESCTune(args.tune)
        print(tune.blheli_32_tune())
